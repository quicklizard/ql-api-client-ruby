require 'net/http'
require 'openssl'
require 'json'

module QL
  module Api
    module HTTP
  
      # QL::Api::HTTP.http_get(host, endpoint, key, digest) -> response
      #
      # Perform HTTP GET request to API at host/endpoint
      #
      # === Example
      #
      #      host = 'https://api.quicklizard.com'
      #      endpoint = '/api/v2/products/confirmed?page=1&period=15.mins&qts=1499072903981'
      #      api_key = 'key'
      #      digest = '84a69330f7e5fd489d997d6ce7e961d2a2d474040477cdad1571a298fc420b79'
      #      QL::Api::HTTP.http_get(host, endpoint, key, digest)
      #      #=> #<Net::HTTPResponse>
      def http_get(host, endpoint, key, digest)
        uri = URI.parse("#{host}#{endpoint}")
        http = Net::HTTP.new(uri.host, uri.port)
        if uri.scheme == 'https'
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        end
        request = Net::HTTP::Get.new(endpoint)
        request['API_KEY'] = key
        request['API_DIGEST'] = digest
        http.request(request)
      end

      # QL::Api::HTTP.http_delete(host, endpoint, key, digest) -> response
      #
      # Perform HTTP DELETE request to API at host/endpoint
      #
      # === Example
      #
      #      host = 'https://api.quicklizard.com'
      #      endpoint = '/api/v3/data_sources/12345?qts=1499072903981'
      #      api_key = 'key'
      #      digest = '84a69330f7e5fd489d997d6ce7e961d2a2d474040477cdad1571a298fc420b79'
      #      QL::Api::HTTP.http_delete(host, endpoint, key, digest)
      #      #=> #<Net::HTTPResponse>
      def http_delete(host, endpoint, key, digest)
        uri = URI.parse("#{host}#{endpoint}")
        http = Net::HTTP.new(uri.host, uri.port)
        if uri.scheme == 'https'
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        end
        request = Net::HTTP::Delete.new(endpoint)
        request['API_KEY'] = key
        request['API_DIGEST'] = digest
        http.request(request)
      end

      # QL::Api::HTTP.http_post(host, endpoint, payload, key, digest) -> response
      #
      # Perform HTTP POST request to API at host/endpoint
      #
      # === Example
      #
      #      host = 'https://api.quicklizard.com'
      #      endpoint = '/api/v2/products/bulk_create?qts=1499072903981'
      #      payload = {products: [ {client_uid: 'abc', price: 12.2} ]}
      #      api_key = 'key'
      #      digest = '84a69330f7e5fd489d997d6ce7e961d2a2d474040477cdad1571a298fc420b79'
      #      QL::Api::HTTP.http_post(host, endpoint, payload, key, digest)
      #      #=> #<Net::HTTPResponse>
      def http_post(host, endpoint, payload, key, digest, content_type = 'application/json')
        uri = URI.parse("#{host}#{endpoint}")
        http = Net::HTTP.new(uri.host, uri.port)
        if uri.scheme == 'https'
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        end
        if payload.is_a?(String)
          request = Net::HTTP::Post.new(endpoint)
          request.body = payload
        elsif payload.respond_to?(:to_json)
          request = Net::HTTP::Post.new(endpoint)
          request.body = payload.to_json
        else
          raise StandardError.new('payload cannot be serialized')
        end
        request['Content-Type'] = content_type
        request['API_KEY'] = key
        request['API_DIGEST'] = digest
        http.request(request)
      end

      # QL::Api::HTTP.http_put(host, endpoint, payload, key, digest) -> response
      #
      # Perform HTTP PUT request to API at host/endpoint
      #
      # === Example
      #
      #      host = 'https://api.quicklizard.com'
      #      endpoint = '/api/v2/products/bulk_create?qts=1499072903981'
      #      payload = {products: [ {client_uid: 'abc', price: 12.2} ]}
      #      api_key = 'key'
      #      digest = '84a69330f7e5fd489d997d6ce7e961d2a2d474040477cdad1571a298fc420b79'
      #      QL::Api::HTTP.http_put(host, endpoint, payload, key, digest)
      #      #=> #<Net::HTTPResponse>
      def http_put(host, endpoint, payload, key, digest)
        uri = URI.parse("#{host}#{endpoint}")
        http = Net::HTTP.new(uri.host, uri.port)
        if uri.scheme == 'https'
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        end
        if payload.respond_to?(:to_json)
          request = Net::HTTP::Put.new(endpoint, 'Content-Type' => 'application/json')
          request.body = payload.to_json
        else
          request = Net::HTTP::Put.new(endpoint)
          request.body = payload
        end
        request['API_KEY'] = key
        request['API_DIGEST'] = digest
        http.request(request)
      end
    
    end
  end
end
