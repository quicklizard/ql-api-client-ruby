require 'digest'

module QL
  module Api
    module Authentication
  
      # QL::Api::Authentication.sign_request(api_secret, path, qs: nil, body: nil) -> aString
      #
      # Returns the API request parts signature, which is a SHA256 of path, query-string, body and API secret
      #
      # === Example
      #
      #      api_secret = 'secret'
      #      path = '/api/v2/products/bulk_update'
      #      qs = 'qts=1499072903981'
      #      body = {products: [ {client_uid: 'abc', price: 12.2} ]}
      #
      #      digest = QL::Api::Authentication.sign_request(api_secret, path, qs: qs, body: body)
      #      #=> "84a69330f7e5fd489d997d6ce7e961d2a2d474040477cdad1571a298fc420b79"
      def sign_request(api_secret, path, qs: nil, body: nil)
        parts = [ path ]
        _body = body.is_a?(Hash) ? body.to_json : body
        parts << qs unless qs.nil?
        parts << _body unless _body.nil?
        parts << api_secret
        Digest::SHA256.hexdigest(parts.join)
      end
      
    end
  end
end
