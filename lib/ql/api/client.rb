require 'uri'
require 'ql/api/authentication'
require 'ql/api/http'
require 'securerandom'

module QL
  module Api
    class Client
      
      include Authentication
      include HTTP
      
      attr_accessor :host, :key, :secret

      # QL::Api::Client.new(key, secret) -> client
      #
      # Create a new instance of Quicklizard's API Client
      #
      # === Example
      #
      #      host = 'https://rest.quicklizard.com'
      #      api_key = 'key'
      #      api_secret = 'secret'
      #      client = QL::Api::Client.new(host, api_key, api_secret)
      #      #=> #<QL::Api::Client:0x007f8b163b1570 @host="https://rest.quicklizard.com" @key="key", @secret="secret">
      def initialize(host, key, secret)
        self.host = host
        self.key = key
        self.secret = secret
      end

      # client.get(endpoint) -> Net::HTTPResponse
      #
      # Perform an HTTP GET request to API endpoint
      #
      # === Example
      #
      #      endpoint = '/api/v2/recommendations/accepted?page=1&period=15.mins'
      #      api_key = 'key'
      #      api_secret = 'secret'
      #      client = QL::Api::Client.new('https://rest.quicklizard.com', api_key, api_secret)
      #      client.get(endpoint)
      #      #=> #<Net::HTTPResponse>
      def get(endpoint)
        uri = get_uri(endpoint)
        digest = sign_request(secret, uri.path, qs: uri.query)
        api_endpoint = get_api_endpoint(uri.path, uri.query)
        http_get(self.host, api_endpoint, key, digest)
      end

      # client.post(endpoint, payload) -> Net::HTTPResponse
      #
      # Perform an HTTP POST request to API endpoint
      #
      # === Example
      #
      #      endpoint = '/api/v2/products/create'
      #      payload = {payload: [ {client_uid: 'abc', price: 12.2} ]}
      #      api_key = 'key'
      #      api_secret = 'secret'
      #      client = QL::Api::Client.new('https://rest.quicklizard.com', api_key, api_secret)
      #      client.post(endpoint, payload)
      #      #=> #<Net::HTTPResponse>
      def post(endpoint, payload)
        uri = get_uri(endpoint)
        digest = sign_request(secret, uri.path, qs: uri.query, body: payload)
        api_endpoint = get_api_endpoint(uri.path, uri.query)
        http_post(self.host, api_endpoint, payload, key, digest)
      end

      # client.put(endpoint, payload) -> Net::HTTPResponse
      #
      # Perform an HTTP PUT request to API endpoint
      #
      # === Example
      #
      #      endpoint = '/api/v2/products/update'
      #      payload = {payload: [ {client_uid: 'abc', price: 12.2} ]}
      #      api_key = 'key'
      #      api_secret = 'secret'
      #      client = QL::Api::Client.new('https://rest.quicklizard.com', api_key, api_secret)
      #      client.post(endpoint, payload)
      #      #=> #<Net::HTTPResponse>
      def put(endpoint, payload)
        uri = get_uri(endpoint)
        digest = sign_request(secret, uri.path, qs: uri.query, body: payload)
        api_endpoint = get_api_endpoint(uri.path, uri.query)
        http_put(self.host, api_endpoint, payload, key, digest)
      end

      # client.upload(endpoint, local_file) -> Net::HTTPResponse
      #
      # Upload a file to API endpoint
      #
      # === Example
      #
      #      endpoint = '/api/v2/products/upload'
      #      local_file = '/tmp/products.txt'
      #      api_key = 'key'
      #      api_secret = 'secret'
      #      client = QL::Api::Client.new('https://rest.quicklizard.com', api_key, api_secret)
      #      client.upload(endpoint, local_file)
      #      #=> #<Net::HTTPResponse>
      def upload(endpoint, local_file, field_name = 'upload')
        uri = get_uri(endpoint)
        digest = sign_request(secret, uri.path, qs: uri.query)
        api_endpoint = get_api_endpoint(uri.path, uri.query)
        body, content_type = get_multipart_body(local_file, field_name)
        http_post(self.host, api_endpoint, body, key, digest, content_type)
      end

      # client.delete(endpoint) -> Net::HTTPResponse
      #
      # Perform an HTTP DELETE request to API endpoint
      #
      # === Example
      #
      #      endpoint = '/api/v3/data_sources/12345'
      #      api_key = 'key'
      #      api_secret = 'secret'
      #      client = QL::Api::Client.new('https://rest.quicklizard.com', api_key, api_secret)
      #      client.delete(endpoint)
      #      #=> #<Net::HTTPResponse>
      def delete(endpoint)
        uri = get_uri(endpoint)
        digest = sign_request(secret, uri.path, qs: uri.query)
        api_endpoint = get_api_endpoint(uri.path, uri.query)
        http_delete(self.host, api_endpoint, key, digest)
      end
      
      private
      
      # Adds a query timestamp parameter to API endpoint and returns as URI
      #
      # === Example
      #
      #      endpoint = '/api/v2/recommendations/all?page=1'
      #      get_uri(endpoint)
      #      #=> #<URI::HTTPS https://rest.quicklizard.com/api/v2/products?page=1&qts=1499072903981>
      def get_uri(endpoint)
        now = Time.now.utc.to_i * 1000
        if endpoint.include?('?')
          endpoint += "&qts=#{now}"
        else
          endpoint += "?qts=#{now}"
        end
        URI.parse("#{self.host}#{endpoint}")
      end

      # Joins API request path and query-string
      #
      # === Example
      #
      #      endpoint = '/api/v2/recommendations/all'
      #      qs = 'page=1'
      #      get_api_endpoint(endpoint)
      #      #=> '/api/v2/products?page=1'
      def get_api_endpoint(path, qs)
        [path, qs].compact.join('?')
      end
      
      # Creates a multipart body for HTTP file upload
      def get_multipart_body(local_file, field_name = 'upload')
        boundary = SecureRandom.hex(16)
        post_body = []
        post_body << "--#{boundary}\r\n"
        post_body << "Content-Disposition: form-data; name=\"#{field_name}\"; filename=\"#{File.basename(local_file)}\"\r\n"
        post_body << "Content-Type: text/plain\r\n\r\n"
        post_body << "#{File.read(local_file)}\r\n"
        post_body << "--#{boundary}\r\n"
        [post_body.join, "multipart/form-data; boundary=#{boundary}"]
      end
      
    end
  end
end
