require 'spec_helper'

RSpec.describe QL::Api::Client do
  
  let(:client) {
    QL::Api::Client.new('https://rest.quicklizard.com', ENV['API_KEY'], ENV['API_SECRET'])
  }
  
  describe '#get' do
  
    context 'with valid endpoint' do
  
      subject(:response) {
        client.get('/api/v2/products')
      }

      it 'has a 200 status code' do
        expect(response.code).to eq('200')
      end

      it 'has cotent-type application/json' do
        expect(response['content-type']).to include('application/json')
      end
      
    end
    
  end
  
  describe '#post' do
  
    context 'with valid payload' do
  
      subject(:response) {
        client.post('/api/v2/products/create', {products: [{product_id: nil}]})
      }

      it 'has a 200 status code' do
        expect(response.code).to eq('200')
      end

      it 'has cotent-type application/json' do
        expect(response['content-type']).to include('application/json')
      end
      
    end

    context 'with invalid payload' do
  
      subject(:response) {
        client.post('/api/v2/products/create', {products: []})
      }
  
      it 'has a 500 status code' do
        expect(response.code).to eq('500')
      end
  
      it 'has errors in response' do
        response_body = JSON.parse(response.body)
        expect(response_body['message']).not_to be(nil)
      end

    end
    
  end
  
  describe '#put' do
  
    context 'with valid payload' do
  
      subject(:response) {
        client.put('/api/v2/products/update', {products: [{product_id: nil}]})
      }
    
      it 'has a 200 status code' do
        expect(response.code).to eq('200')
      end
    
      it 'has cotent-type application/json' do
        expect(response['content-type']).to include('application/json')
      end
  
    end
  
    context 'with invalid payload' do
  
      subject(:response) {
        client.put('/api/v2/products/update', {prod: []})
      }
    
      it 'has a 500 status code' do
        expect(response.code).to eq('500')
      end
    
      it 'has errors in response' do
        response_body = JSON.parse(response.body)
        expect(response_body['message']).not_to be(nil)
      end
  
    end
    
  end
  
  describe '#upload' do
  
    context 'with valid payload' do
  
      subject(:response) {
        client.upload('/api/v2/products/upload?client_key=test_client', '/tmp/upload_template.txt')
      }
    
      it 'has a 200 status code' do
        expect(response.code).to eq('200')
      end
    
      it 'has cotent-type application/json' do
        expect(response['content-type']).to include('application/json')
      end
    
      it 'was uploaded successfully' do
        response_body = JSON.parse(response.body)
        expect(response_body['message']).to include('file uploaded as test_client')
      end
  
    end
    
  end
  
end
