require 'spec_helper'

RSpec.describe QL::Api::Authentication do
  
  let(:client) {
    QL::Api::Client.new('https://rest.quicklizard.com', ENV['API_KEY'], ENV['API_SECRET'])
  }

  context '#sign_request with request query string' do
    
    it 'creates a valid signature' do
      path = '/api/v2/products'
      qs = 'page=1&per_page=50'
      signature = client.sign_request(ENV['API_SECRET'], path, qs: qs)
      expect(signature).to eq('884d2fc11eb14ce3867c65be78358799cd144f6d1d93c09cd1c244bf805fbfed')
    end
    
  end

  context '#sign_request with request body as JSON' do
  
    it 'creates a valid signature' do
      path = '/api/v2/bulk_update'
      qs = 'qts=0123456789'
      payload = {products: []}
      signature = client.sign_request(ENV['API_SECRET'], path, qs: qs, body: payload)
      expect(signature).to eq('b0408cbc9ade979c92edf1348592305ebb8b9cab41cfc5b223a29575849fd21f')
    end
    
  end

  context '#sign_request with request body as string' do
  
    it 'creates a valid signature' do
      path = '/api/v2/bulk_update'
      qs = 'qts=0123456789'
      payload = 'products[][]client_uid=123'
      signature = client.sign_request(ENV['API_SECRET'], path, qs: qs, body: payload)
      expect(signature).to eq('1aac75367b3820ed73db8cf261a95403dacdfd80e759f56076b386a43ab8ce49')
    end
    
  end
  
end