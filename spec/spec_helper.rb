require 'bundler/setup'
require 'ql/api'

credentials = File.expand_path('../../.api_credentials', __FILE__)

if File.exists?(credentials)
  File.readlines(credentials).each do |line|
    key, value = line.split('=')
    ENV["API_#{key.strip}"] = value.strip
  end
end

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
