# Quicklizard API Client - Ruby

Authenticate and perform operations against Quicklizard's REST API from your Ruby application.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'ql-api', git: 'git@bitbucket.org:quicklizard/ql-api-client-ruby.git'
```

And then execute:

    $ bundle

## Usage

```ruby
require 'json'
require 'ql-api'
# replace API_KEY and API_SECRET with your Quicklizard API Credentials
client = QL::Api::Client.new('https://rest.quicklizard.com', API_KEY, API_SECRET)

response = client.get('/api/v3/recommendations/all?page=1&per_page=50') #=> '{"total": 100, "result": []}'
data = JSON.parse(response)
puts data['total'] #=> 100
data['result'].each do |product|
  puts product.inspect #=> {'uid' => '2asds23231sadxxsdqweqwqw' ... }
end
```

### GET Requests

```ruby
require 'json'
require 'ql-api'
# replace API_KEY and API_SECRET with your Quicklizard API Credentials
client = QL::Api::Client.new('https://rest.quicklizard.com', API_KEY, API_SECRET)

response = client.get('/api/v3/recommendations/all?page=1&per_page=50') #=> '{"total": 100, "result": []}'
```
---
### POST Requests

```ruby
require 'json'
require 'ql-api'
# replace API_KEY and API_SECRET with your Quicklizard API Credentials
client = QL::Api::Client.new('https://rest.quicklizard.com', API_KEY, API_SECRET)

payload = {
  payload: [
    {product_id: '1234', ....} # see API docs for complete structure
  ]
}
response = client.post('/api/v3/products/create', payload)
```
---
### PUT Requests

```ruby
require 'json'
require 'ql-api'
# replace API_KEY and API_SECRET with your Quicklizard API Credentials
client = QL::Api::Client.new('https://rest.quicklizard.com', API_KEY, API_SECRET)

payload = {
  payload: [
    {product_id: '1234', ....} # see API docs for complete structure
  ]
}
response = client.put('/api/v3/products/update', payload)
```
---

### DELETE Requests

```ruby
require 'json'
require 'ql-api'
# replace API_KEY and API_SECRET with your Quicklizard API Credentials
client = QL::Api::Client.new('https://rest.quicklizard.com', API_KEY, API_SECRET)

response = client.delete('/api/v3/data_sources/12345')
```
---

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/quicklizard/ql-api-client-ruby.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

